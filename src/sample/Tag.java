package sample;

import java.io.Serializable;
import java.util.ArrayList;

public class Tag implements Serializable {

    static final long serialVersionUID = 1L;
    private String tagName;
    private ArrayList<String> tagValues;

    public Tag(String tagName) {
        this.tagName = tagName;
        this.tagValues = new ArrayList<>();
    }

    public String getTagName() {
        return tagName;
    }

    public ArrayList<String> getTagValues() {
        return this.tagValues;
    }

    public void addTagValue(String tagValue) {
        this.tagValues.add(tagValue);
    }

    public void removeTagValue(String tagValue) {
        this.tagValues.removeIf(tagValue::equals);
    }
}
