package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.*;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class AdminController implements Initializable {

    private UserList users;

    public AdminController() {
        this.users = new UserList();
    }

    @FXML
    private ListView<String> userListView;

    @FXML
    private TextField usernameInput;

    String currentUser;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            this.users = UserList.getUsers();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        if (!users.isEmpty()) {
            ArrayList<String> usernames = users.getUsernames();

            userListView.getItems().addAll(usernames);

            /*userListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                    currentSong = library.getSelectionModel().getSelectedItem();
                    if (currentSong != null) {
                        String[] songParts = currentSong.split("\\|");

                        Song obj = list.searchSong(songParts[0].trim(), songParts[1].trim());
                        setLabels(obj);
                    }
                }
            });*/

            if (!userListView.getItems().isEmpty()) {
                userListView.getSelectionModel().select(0);
            }
        }

    }

    @FXML
    public void add(javafx.scene.input.MouseEvent mouseEvent) throws IOException {
        String newUsername = usernameInput.getText();

        if (newUsername.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot leave this field blank", ButtonType.OK);
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to add this user?", ButtonType.YES, ButtonType.CANCEL);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                userListView.getItems().add(newUsername);

                this.users.add(new User(newUsername));
                UserList.setUsers(users);
            }
        }
    }

    @FXML
    public void edit(javafx.scene.input.MouseEvent mouseEvent) throws IOException {
        String newUsername = usernameInput.getText();

        int selectedID = userListView.getSelectionModel().getSelectedIndex();

        if (newUsername.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot leave this field blank", ButtonType.OK);
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to edit this user?", ButtonType.YES, ButtonType.CANCEL);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                String selectedUsername = userListView.getItems().get(selectedID);
                this.users.edit(selectedUsername, newUsername);

                userListView.getItems().set(selectedID, newUsername);
                UserList.setUsers(users);
            }
        }
    }

    @FXML
    public void delete(javafx.scene.input.MouseEvent mouseEvent) throws IOException {
        int selectedID = userListView.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete this user?", ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            String selectedUsername = userListView.getItems().get(selectedID);
            this.users.remove(selectedUsername);

            userListView.getItems().remove(selectedID);
            UserList.setUsers(users);

            if (userListView.getItems().isEmpty()) {
                usernameInput.setText("");
            }
        }
    }

}
