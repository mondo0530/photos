package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;

public class PhotoController implements Initializable {

    private UserList users;
    private User user;
    private Album album;

    @FXML
    private Label albumLabel;

    @FXML
    private TilePane photoPane;

    @FXML
    private TextField photoInput;

    private Stage stage;
    private Scene scene;
    private Parent root;

    public PhotoController(UserList users, User user, Album album) {
        this.users = users;
        this.user = user;
        this.album = album;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        photoPane.setPadding(new Insets(15, 15, 15, 15));
        photoPane.setHgap(15);
        photoPane.setPrefTileHeight(200);
        photoPane.setPrefTileWidth(200);

        albumLabel.setText("Welcome " + album.getName());

        ArrayList<File> photos = album.getPhotoFiles();
        for (File f: photos) {
            VBox box = new VBox();
            box.setMaxSize(200, 200);

            ImageView imageView;
            imageView = createImageView(f);
            imageView.setFitWidth(150);
            imageView.setFitHeight(150);

            Label label = new Label();
            label.setText(album.getPhoto(f.getName()).getCaption());

            box.getChildren().addAll(imageView,label);
            photoPane.getChildren().addAll(box);
        }

        /*if (!photos.isEmpty()) {
            ArrayList<String> output = new ArrayList<>();
            for (File f: photos) {
                output.add(f.getName());
            }

            photoListView.getItems().addAll(output);

            *//*userListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                    currentSong = library.getSelectionModel().getSelectedItem();
                    if (currentSong != null) {
                        String[] songParts = currentSong.split("\\|");

                        Song obj = list.searchSong(songParts[0].trim(), songParts[1].trim());
                        setLabels(obj);
                    }
                }
            });*//*

            if (!photoListView.getItems().isEmpty()) {
                photoListView.getSelectionModel().select(0);
            }
        }*/
    }

    private ImageView createImageView(final File imageFile) {
        // DEFAULT_THUMBNAIL_WIDTH is a constant you need to define
        // The last two arguments are: preserveRatio, and use smooth (slower)
        // resizing

        ImageView imageView = null;
        try {
            final Image image = new Image(new FileInputStream(imageFile), 150, 0, true, true);
            long time1 = imageFile.lastModified();
            DateFormat ex = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");
            imageView = new ImageView(image);
            imageView.setFitWidth(150);
            imageView.setOnMouseClicked(new EventHandler<MouseEvent>() {

                @Override
                public void handle(MouseEvent mouseEvent) {

                    if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){

                        if(mouseEvent.getClickCount() == 1){
                            try {
                                ExpandedPhotoController expandedPhotoController = new ExpandedPhotoController(users, user, album, album.getPhoto(imageFile.getName()));

                                FXMLLoader loader = new FXMLLoader(getClass().getResource("expandedphoto.fxml"));
                                loader.setController(expandedPhotoController);

                                //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
                                root = loader.load();
                                //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
                                //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
                                stage = (Stage)((Node)mouseEvent.getSource()).getScene().getWindow();
                                scene = new Scene(root);
                                stage.setScene(scene);
                                stage.centerOnScreen();
                                stage.show();
                                /*BorderPane borderPane = new BorderPane();
                                ImageView imageView = new ImageView();
                                Image image = new Image(new FileInputStream(imageFile));
                                imageView.setImage(image);
                                imageView.setStyle("-fx-background-color: BLACK");
                                imageView.setFitHeight(stage.getHeight() - 10);
                                imageView.setPreserveRatio(true);
                                imageView.setSmooth(true);
                                imageView.setCache(true);
                                borderPane.setCenter(imageView);
                                borderPane.setStyle("-fx-background-color: BLACK");
                                Stage newStage = new Stage();
                                newStage.setWidth(stage.getWidth());
                                newStage.setHeight(stage.getHeight());
                                newStage.setTitle(imageFile.getName());
                                Scene scene = new Scene(borderPane, Color.BLACK);
                                newStage.setScene(scene);
                                newStage.show();*/
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
            });
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return imageView;
    }

    /*@FXML
    private void expandPhoto(ActionEvent event) throws IOException {
        int selectedID = photoListView.getSelectionModel().getSelectedIndex();
        String selectedPhoto = photoListView.getItems().get(selectedID);
        ExpandedPhotoController expandedPhotoController = new ExpandedPhotoController(users, album.getPhoto(selectedPhoto));

        FXMLLoader loader = new FXMLLoader(getClass().getResource("expandedphoto.fxml"));
        loader.setController(expandedPhotoController);

        //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
        root = loader.load();
        //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }*/

    @FXML
    public void addPhoto() throws IOException {
        FileChooser fc = new FileChooser();
        fc.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png"));
        File selectedFile = fc.showOpenDialog(null);

        if (selectedFile != null) {
            Photo newPhoto = new Photo(selectedFile.getPath(), "");
            File imageFile = newPhoto.getImageFile();
            album.addPhoto(newPhoto);
            VBox box = new VBox();
            box.setMaxSize(200, 200);

            ImageView imageView;
            imageView = createImageView(imageFile);
            imageView.setFitWidth(150);
            imageView.setFitHeight(150);

            Label label = new Label();
            label.setText(album.getPhoto(imageFile.getName()).getCaption());

            box.getChildren().addAll(imageView,label);
            photoPane.getChildren().addAll(box);
            UserList.setUsers(users);
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Invalid file type." , ButtonType.OK);
            alert.showAndWait();
        }
    }

    @FXML private void back(ActionEvent event) throws IOException {
        AlbumController albumController = new AlbumController(users, user);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("albums.fxml"));
        loader.setController(albumController);

        //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
        root = loader.load();
        //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML private void logout(ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you'd like to exit the application?" , ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));

            //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
            root = loader.load();
            //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
        }
    }

    @FXML private void exit(ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you'd like to exit the application?" , ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.close();
        }
    }
}
