package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class LoginController implements Initializable {

    private UserList users;

    private Stage stage;
    private Scene scene;
    private Parent root;

    @FXML
    private TextField username;

    public LoginController() {
        users = new UserList();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            users = UserList.getUsers();
            if (users.searchUsers("stock") == null) {
                System.out.println("here");
                Album stockAlbum = new Album("Stock");

                Photo selfie = new Photo("src/sample/photos/akselfie.jpg", "ak selfie");
                stockAlbum.addPhoto(selfie);
                Tag akTag = new Tag("people");
                akTag.addTagValue("girl");
                akTag.addTagValue("ak");
                selfie.getTags().add(akTag);
                Tag locationTag = new Tag("location");
                locationTag.addTagValue("idk");
                locationTag.addTagValue("google");
                selfie.getTags().add(locationTag);

                Photo crazy = new Photo("src/sample/photos/crazy.jpg","crazy");
                stockAlbum.addPhoto(crazy);
                Tag crazyTag = new Tag("people");
                crazyTag.addTagValue("girl");
                crazyTag.addTagValue("goldfish");
                crazyTag.addTagValue("pistol");
                crazy.getTags().add(crazyTag);

                Photo horse = new Photo("src/sample/photos/drunk_horse.jpg","drunk horse");
                stockAlbum.addPhoto(horse);
                Tag horseTag = new Tag("people");
                horseTag.addTagValue("horse");
                horseTag.addTagValue("empty bottle");
                horse.getTags().add(horseTag);


                Photo grandma = new Photo("src/sample/photos/grandma.jpg","grandma");
                Tag grandmaTag = new Tag("people");
                grandmaTag.addTagValue("grandma");
                grandmaTag.addTagValue("pistol");
                grandma.getTags().add(grandmaTag);
                stockAlbum.addPhoto(grandma);

                Photo santa = new Photo("src/sample/photos/santa.jpg","santa");
                stockAlbum.addPhoto(santa);

                Photo wtf = new Photo("src/sample/photos/wtf.jpg","wtf");
                stockAlbum.addPhoto(wtf);


                User stock = new User("stock");
                stock.addAlbum(stockAlbum);

                users.add(stock);

                UserList.setUsers(users);

                users = UserList.getUsers();
            }
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void validate(ActionEvent event) throws IOException {
        //make sure there's always a stock user in the db
        if (users.searchUsers("stock") != null) {
            /*Album stockAlbum = new Album("Stock");

            Photo selfie = new Photo("src/sample/photos/akselfie.jpg", "ak selfie");
            stockAlbum.addPhoto(selfie);
            Tag akTag = new Tag("people");
            akTag.addTagValue("girl");
            akTag.addTagValue("ak");
            selfie.getTags().add(akTag);
            Tag locationTag = new Tag("location");
            locationTag.addTagValue("idk");
            locationTag.addTagValue("google");
            selfie.getTags().add(locationTag);

            Photo crazy = new Photo("src/sample/photos/crazy.jpg","crazy");
            stockAlbum.addPhoto(crazy);
            Tag crazyTag = new Tag("people");
            crazyTag.addTagValue("girl");
            crazyTag.addTagValue("goldfish");
            crazyTag.addTagValue("pistol");
            crazy.getTags().add(crazyTag);

            Photo horse = new Photo("src/sample/photos/drunk_horse.jpg","drunk horse");
            stockAlbum.addPhoto(horse);
            Tag horseTag = new Tag("people");
            horseTag.addTagValue("horse");
            horseTag.addTagValue("empty bottle");
            horse.getTags().add(horseTag);


            Photo grandma = new Photo("src/sample/photos/grandma.jpg","grandma");
            Tag grandmaTag = new Tag("people");
            grandmaTag.addTagValue("grandma");
            grandmaTag.addTagValue("pistol");
            grandma.getTags().add(grandmaTag);
            stockAlbum.addPhoto(grandma);

            Photo huh = new Photo("src/sample/photos/huh.jpg","huh");
            Tag huhTag = new Tag("people");
            huhTag.addTagValue(" pregnant women");
            huhTag.addTagValue("unborn baby");
            huhTag.addTagValue("pistol");
            huh.getTags().add(huhTag);
            stockAlbum.addPhoto(huh);

            Photo santa = new Photo("src/sample/photos/santa.jpg","santa");
            stockAlbum.addPhoto(santa);

            Photo wtf = new Photo("src/sample/photos/wtf.jpg","wtf");
            stockAlbum.addPhoto(wtf);


            User stock = new User("stock");
            stock.addAlbum(stockAlbum);

            users.add(stock);

            UserList.setUsers(users);*/
        }

        String user = username.getText();

        if (user.equals("admin")) {
            //switch stage to admin page
            loginToAdmin(event);
        } else {
            if (this.users.searchUsers(user) != null) {
                loginToUser(event, user);
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "User does not exist", ButtonType.OK);
                alert.showAndWait();
            }
        }
    }

    public void loginToAdmin(ActionEvent event) throws IOException {
        root = FXMLLoader.load(getClass().getResource("admin.fxml"));
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    public void loginToUser(ActionEvent event, String username) throws IOException {
        User user = users.searchUsers(username);
        AlbumController albumController = new AlbumController(users, user);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("albums.fxml"));
        loader.setController(albumController);

        //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
        root = loader.load();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }
}
