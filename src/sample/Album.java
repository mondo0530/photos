package sample;

import java.io.File;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Album implements Serializable {

    static final long serialVersionUID = 1L;
    private String name;
    private ArrayList<Photo> photos;

    public Album(String name) {
        this.name = name;
        this.photos = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean addPhoto(Photo photo) {
        if (!photoExistsInAlbum(photo)) {
            this.photos.add(photo);
            return true;
        }
        return false;
    }

    private boolean photoExistsInAlbum(Photo photo) {
        for (Photo p: this.photos) {
            if (p.getImageFile().equals(photo.getImageFile())) {
                return true;
            }
        }
        return false;
    }

    public void removePhoto(Photo photo) {
        this.photos.removeIf(photo::equals);
    }

    public Photo getPhoto(String filename) {
        for (Photo p: this.photos) {
            if (p.getImageFile().getName().equals(filename))
                return p;
        }
        return null;
    }

    public int getPhotoPosition(Photo photo) {
        int i = 0;
        for (Photo p: this.photos) {
            if (p.getImageFile().equals(photo.getImageFile())) {
                break;
            }
            i++;
        }
        return i;
    }

    public Photo getNextPhoto(Photo photo) {
        if (getPhotoPosition(photo) < (photos.size()-1)) {
            return photos.get((getPhotoPosition(photo) + 1));
        }
        return null;
    }

    public Photo getPrevPhoto(Photo photo) {
        if (getPhotoPosition(photo) > 0) {
            return photos.get((getPhotoPosition(photo) - 1));
        }
        return null;
    }

    public ArrayList<File> getPhotoFiles() {
        ArrayList<File> photoFiles = new ArrayList<>();
        for (Photo p: this.photos) {
            File f = new File(p.getFilename());
            photoFiles.add(f);
        }
        return photoFiles;
    }

    public int getSize() {
        return photos.size();
    }

    public void printPhotos() {
        this.photos.forEach(System.out::println);
    }

    public ArrayList<Photo> getPhotos() {
        return this.photos;
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                '}';
    }
}
