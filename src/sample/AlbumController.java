package sample;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ListView;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;
import java.lang.reflect.Array;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import java.net.URL;
import java.util.ResourceBundle;

public class AlbumController implements Initializable {

    private UserList users;
    private User currentUser;

    @FXML
    private Label currentUsername;

    @FXML
    private Label selectedAlbum;

    @FXML
    private ListView<String> albumListView;

    @FXML
    private TextField albumInput;

    private Stage stage;
    private Scene scene;
    private Parent root;

    private String currentAlbum;

    public AlbumController(UserList users, User currentUser) {
        this.users = users;
        this.currentUser = currentUser;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        currentUsername.setText("Welcome " + currentUser.getUsername());

        ArrayList<Album> albums = currentUser.getAlbumList();

        if (!albums.isEmpty()) {
            ArrayList<String> output = new ArrayList<>();
            for (Album a: albums) {
                output.add(a.getName());
                System.out.println(a.getName());
            }

            albumListView.getItems().addAll(output);

            albumListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                    currentAlbum = albumListView.getSelectionModel().getSelectedItem();
                    if (currentAlbum != null) {
                        selectedAlbum.setText(currentAlbum);
                    }
                }
            });

            /*userListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
                @Override
                public void changed(ObservableValue<? extends String> observableValue, String s, String t1) {
                    currentSong = library.getSelectionModel().getSelectedItem();
                    if (currentSong != null) {
                        String[] songParts = currentSong.split("\\|");

                        Song obj = list.searchSong(songParts[0].trim(), songParts[1].trim());
                        setLabels(obj);
                    }
                }
            });*/

            if (!albumListView.getItems().isEmpty()) {
                albumListView.getSelectionModel().select(0);
            }
        }
    }

    public void add() throws IOException {
        String newAlbum = albumInput.getText();

        if (newAlbum.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot leave this field blank", ButtonType.OK);
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to add this user?", ButtonType.YES, ButtonType.CANCEL);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                albumListView.getItems().add(newAlbum);

                this.currentUser.addAlbum(new Album(newAlbum));
                UserList.setUsers(users);
            }
        }
    }

    public void edit() throws IOException {
        String newAlbum = albumInput.getText();

        int selectedID = albumListView.getSelectionModel().getSelectedIndex();

        if (newAlbum.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Cannot leave this field blank", ButtonType.OK);
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to edit this user?", ButtonType.YES, ButtonType.CANCEL);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {
                String selectedAlbum = albumListView.getItems().get(selectedID);
                this.currentUser.getAlbum(selectedAlbum).setName(newAlbum);

                albumListView.getItems().set(selectedID, newAlbum);
                UserList.setUsers(users);
            }
        }
    }

    public void delete() throws IOException {
        int selectedID = albumListView.getSelectionModel().getSelectedIndex();
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete this user?", ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            String selectedAlbum = albumListView.getItems().get(selectedID);
            this.currentUser.removeAlbum(selectedAlbum);

            albumListView.getItems().remove(selectedID);
            UserList.setUsers(users);

            if (albumListView.getItems().isEmpty()) {
                albumInput.setText("");
            }
        }
    }

    /*public void openAlbum() throws IOException {
        int selectedID = albumListView.getSelectionModel().getSelectedIndex();
        String selectedAlbum = albumListView.getItems().get(selectedID);
        PhotoController photoController = new PhotoController(users, currentUser.getAlbum(selectedAlbum));

        FXMLLoader loader = new FXMLLoader(getClass().getResource("photos.fxml"));
        loader.setController(photoController);

        //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
        root = loader.load();
        //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }*/

    @FXML private void open(ActionEvent event) throws IOException {
        int selectedID = albumListView.getSelectionModel().getSelectedIndex();
        String selectedAlbum = albumListView.getItems().get(selectedID);
        PhotoController photoController = new PhotoController(users, currentUser, currentUser.getAlbum(selectedAlbum));

        FXMLLoader loader = new FXMLLoader(getClass().getResource("photos.fxml"));
        loader.setController(photoController);

        //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
        root = loader.load();
        //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.centerOnScreen();
        stage.show();
    }

    @FXML private void logout(ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you'd like to log out?" , ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            LoginController loginController = new LoginController();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("login.fxml"));

            //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
            root = loader.load();
            //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
        }

    }

    @FXML private void exit(ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you'd like to exit the application?" , ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage.close();
        }
        UserList.setUsers(users);
    }

}
