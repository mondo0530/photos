package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class EditPhotoController implements Initializable {

    private Stage stage;
    private Scene scene;
    private Parent root;

    private UserList userList;
    private User user;
    private Album album;
    private Photo photo;

    @FXML
    private ImageView image;

    @FXML
    private Label caption;

    @FXML
    private TextField newCaption;

    @FXML
    private TextField newTagName;

    @FXML
    private TextField newTagValue;

    @FXML
    private ChoiceBox<String> tagName;

    @FXML
    private ChoiceBox<String> tagValue;

    public EditPhotoController(UserList userList, User user, Album album, Photo photo) {
        this.userList = userList;
        this.user = user;
        this.album = album;
        this.photo = photo;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            image.setImage(new Image(new FileInputStream(photo.getImageFile())));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        caption.setText(photo.getCaption());

        ArrayList<Tag> tags = this.photo.getTags();
        if (tags.isEmpty()) {
            Tag newTag = new Tag("");
            newTag.addTagValue("");
            tags.add(newTag);
        }

        for (Tag t: tags) {
            tagName.getItems().add(t.getTagName());
        }
        tagName.setValue(tags.get(0).getTagName());

        for (String s: tags.get(0).getTagValues()) {
            tagValue.getItems().add(s);
        }

        tagValue.setValue(tags.get(0).getTagValues().get(0));

        tagName.setOnAction(event -> {
            String currentValue = tagName.getValue();

            ArrayList<String> tagValues = new ArrayList<>();
            for (Tag t: tags) {
                if (t.getTagName().equals(currentValue)) {
                    tagValues = t.getTagValues();
                }
            }

            if (tagValues.isEmpty()) {
                tagValue.getItems().clear();
                tagValue.getItems().add("");
                tagValue.setValue("");
            } else {
                tagValue.getItems().clear();
                tagValue.getItems().addAll(tagValues);
                tagValue.setValue(tagValues.get(0));
            }
        });
    }

    public void editCaption() throws IOException {
        String recaption = newCaption.getText();
        if (recaption.equals("")) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you'd like to enter an empty caption?", ButtonType.YES, ButtonType.CANCEL);
            alert.showAndWait();
            if (alert.getResult() == ButtonType.YES) {
                photo.setCaption(recaption);
                caption.setText(recaption);
                UserList.setUsers(userList);
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to add this caption?", ButtonType.YES, ButtonType.CANCEL);
            alert.showAndWait();
            if (alert.getResult() == ButtonType.YES) {
                photo.setCaption(recaption);
                caption.setText(recaption);
                UserList.setUsers(userList);
            }
        }
    }

    @FXML
    public void deleteTag() throws IOException {
        String deletedName = tagName.getValue();
        String deletedValue = tagValue.getValue();

        if (deletedName.equals("") && deletedValue.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "There is nothing left to delete.", ButtonType.OK);
            alert.showAndWait();
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete this tag?", ButtonType.YES, ButtonType.CANCEL);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.YES) {

                if (photo.removeTag(deletedName, deletedValue)) {
                    ArrayList<Tag> tags = this.photo.getTags();
                    String newTagName = photo.getTags().get(0).getTagName();
                    tagName.setValue(newTagName);
                    ArrayList<String> tagValues = new ArrayList<>();
                    for (Tag t: tags) {
                        if (t.getTagName().equals(newTagName)) {
                            tagValues = t.getTagValues();
                        }
                    }
                    tagValue.getItems().clear();
                    tagValue.getItems().addAll(tagValues);
                    tagValue.setValue(tagValues.get(0));
                } else {
                    ArrayList<Tag> tags = this.photo.getTags();
                    String newTagName = tagName.getValue();
                    ArrayList<String> tagValues = new ArrayList<>();
                    for (Tag t: tags) {
                        if (t.getTagName().equals(newTagName)) {
                            tagValues = t.getTagValues();
                        }
                    }
                    tagValue.getItems().clear();
                    tagValue.getItems().addAll(tagValues);
                    tagValue.setValue(tagValues.get(0));
                }
                UserList.setUsers(userList);
            }
        }
    }

    @FXML
    public void addTag() throws IOException {
        String newName = newTagName.getText();
        String newValue = newTagValue.getText();

        if (photo.tagExists(newName, newValue)) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Tag already exists.", ButtonType.OK);
            alert.showAndWait();
        } else if (newName.equals("") && newValue.equals("")) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "All fields are blank. Please input a tag name or value.", ButtonType.OK);
            alert.showAndWait();
        } else if (newName.equals("")) {
            if (tagName.getValue().equals("")) {
                Alert alert = new Alert(Alert.AlertType.ERROR, "There is currently no tag name to add this value to. Please input a tag name.", ButtonType.OK);
                alert.showAndWait();
            } else {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "This tag value will be added to the current tage name: " + tagName.getValue() + ". Press ok to add the tag.", ButtonType.OK);
                alert.showAndWait();

                if (alert.getResult() == ButtonType.OK) {
                    photo.addTag(tagName.getValue(), newValue);
                    updateTag(tagName.getValue(), newValue);
                }

            }
        } else if (newValue.equals("")) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "This will register a new tag name with no value. It will be available for us but will not tag the photo without a value. Press ok to continue.", ButtonType.OK);
            alert.showAndWait();

            //will add tag name to database
        } else {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to add this new tag?", ButtonType.OK);
            alert.showAndWait();

            if (alert.getResult() == ButtonType.OK) {
                photo.addTag(newName, newValue);
                updateTag(newName, newValue);
            }
        }
        UserList.setUsers(userList);
    }

    private void updateTag(String name, String value) {
        ArrayList<Tag> tags = this.photo.getTags();

        ArrayList<String> tagNames = new ArrayList<>();
        for (Tag t: tags) {
            tagNames.add(t.getTagName());
        }
        //tagName.getItems().clear();
        tagName.getItems().addAll(tagNames);
        tagName.setValue(name);

        ArrayList<String> tagValues = new ArrayList<>();
        for (Tag t: tags) {
            if (t.getTagName().equals(name)) {
                tagValues = t.getTagValues();
            }
        }

        tagValue.getItems().clear();
        tagValue.getItems().addAll(tagValues);
        tagValue.setValue(value);
    }

    @FXML private void done(ActionEvent event) throws IOException {
        ExpandedPhotoController expandedPhotoController = new ExpandedPhotoController(userList, user, album, photo);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("expandedphoto.fxml"));
        loader.setController(expandedPhotoController);

        //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
        root = loader.load();
        //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

}