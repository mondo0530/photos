package sample;

import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class User implements Serializable {

    static final long serialVersionUID = 1L;
    public static final String storeFile = "storage.dat";

    private String username;
    private final ArrayList<Album> albums;

    public User(String username) {
        this.username = username;
        this.albums = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void addAlbum(Album album) {
        this.albums.add(album);
    }

    public void removeAlbum(String albumName) {
        this.albums.removeIf(album -> album.getName().equals(albumName));
    }

    public Album getAlbum(String name) {
        for (Album a: this.albums) {
            if (a.getName().equals(name)) {
                return a;
            }
        }
        return null;
    }

    public int getNumOfAlbums() {
        return this.albums.size();
    }

    public void printAlbums() {
        for (Album a: this.albums) {
            a.printPhotos();
        }
    }

    public ArrayList<String> getTagNames() {
        ArrayList<String> tagNames = new ArrayList<>();
        for (Album a: albums) {
            for (Photo p: a.getPhotos()) {
                for (Tag t: p.getTags()) {
                    if (!tagNames.contains(t.getTagName()) && !t.getTagName().equals("")) {
                        tagNames.add(t.getTagName());
                    }
                }
            }
        }
        return tagNames;
    }

    public ArrayList<String> getTagValues(String tagName) {
        ArrayList<String> tagValues = new ArrayList<>();
        for (Album a: albums) {
            for (Photo p: a.getPhotos()) {
                for (Tag t: p.getTags()) {
                    if (t.getTagName().equals(tagName)) {
                        for (String val: t.getTagValues()) {
                            if (!tagValues.contains(val)) {
                                tagValues.add(val);
                            }
                        }
                    }
                }
            }
        }
        return tagValues;
    }

    public static void writeApp(User user) throws IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storeFile));
        oos.writeObject(user);
    }

    public static User readApp() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storeFile));
        return (User)ois.readObject();
    }

    public ArrayList<Album> getAlbumList() {
        return this.albums;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                '}';
    }
}
