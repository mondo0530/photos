package sample;

import java.io.*;
import java.util.ArrayList;

public class UserList implements Serializable {

    static final long serialVersionUID = 1L;
    public static final String storeFile = "src/sample/storage.dat";
    private ArrayList<User> users;

    public UserList() {
        this.users = new ArrayList<>();
    }

    public void add(User user) {
        this.users.add(user);
    }

    public void edit(String oldUsername, String newUsername) {
        for (User u: this.users) {
            if (u.getUsername().equals(oldUsername)) {
                u.setUsername(newUsername);
            }
        }
    }

    public void remove(String username) {
        users.removeIf(user -> user.getUsername().equals(username));
    }

    public User searchUsers(String username) {
        for (User u: this.users) {
            if (u.getUsername().equals(username)) {
                return u;
            }
        }
        return null;
    }

    public static UserList getUsers() throws IOException, ClassNotFoundException {
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(storeFile));
        return (UserList)ois.readObject();
    }

    public static void setUsers(UserList users) throws IOException, IOException {
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(storeFile));
        oos.writeObject(users);
    }

    public ArrayList<String> getUsernames() {
        ArrayList<String> usernames = new ArrayList<>();
        for (User u: this.users) {
            usernames.add(u.getUsername());
        }
        return usernames;
    }

    public boolean isEmpty() {
        return this.users.isEmpty();
    }

}
