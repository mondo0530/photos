package sample;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class Photo implements Serializable {

    static final long serialVersionUID = 1L;
    private final String filename;
    private String caption;
    private ArrayList<Tag> tags;

    public Photo(String filename, String caption) {
        this.filename = filename;
        this.caption = caption;
        tags = new ArrayList<>();
    }

    public File getImageFile() {
        //BufferedImage image;

        /*try {
            image = ImageIO.read(file);
        }
        catch (IOException ioe) {
            throw new IllegalArgumentException("could not open file: " + file, ioe);
        }
        if (image == null) {
            throw new IllegalArgumentException("could not read file: " + file);
        }*/

        return new File(filename);
    }

    public String getFilename() {
        return this.filename;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getDate() {
        File imageFile = new File(this.filename);
        long time1 = imageFile.lastModified();
        DateFormat ex = new SimpleDateFormat("MMMM dd, yyyy hh:mm a");

        return ex.format(time1);
    }

    public boolean addTag(String name, String value) {
        for (Tag t: tags) {
            if (t.getTagName().equals("")) {
                tags.removeIf(tag -> tag.getTagName().equals(""));
                Tag newTag = new Tag(name);
                newTag.addTagValue(value);
                tags.add(newTag);
                return true;
            }

            if (t.getTagName().equals(name)) {
                for (String v: t.getTagValues()) {
                    if (v.equals(value)) return false;
                }

                t.addTagValue(value);
                return true;
            }
        }
        Tag newTag = new Tag(name);
        if (!value.equals("")) {
            newTag.addTagValue(value);
        }
        tags.add(newTag);

        return true;
    }

    public boolean tagExists(String name, String value) {
        for (Tag t: tags) {
            if (t.getTagName().equals(name)) {
                for (String v: t.getTagValues()) {
                    if (v.equals(value)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private void removeTagName(String name) {
        tags.removeIf(t -> t.getTagName().equals(name));
    }

    public boolean isLastTag(String name, String value) {

        for (Tag t: tags) {
            if (t.getTagName().equals(name)) {
                t.getTagValues().removeIf(v -> v.equals(value));
                if (t.getTagValues().isEmpty()) {
                    removeTagName(name);
                    return true;
                }

            }
        }
        return false;
    }

    public boolean removeTag(String name, String value) {
        for (Tag t: tags) {
            if (t.getTagName().equals(name)) {
                t.getTagValues().removeIf(v -> v.equals(value));
                if (t.getTagValues().isEmpty()) {
                    removeTagName(name);
                    if (tags.isEmpty()) {
                        Tag newTag = new Tag("");
                        newTag.addTagValue("");
                        tags.add(newTag);
                        return true;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    public ArrayList<Tag> getTags() {
        return this.tags;
    }

    @Override
    public String toString() {
        return "Photo{" +
                "caption='" + caption + '\'' +
                '}';
    }
}
