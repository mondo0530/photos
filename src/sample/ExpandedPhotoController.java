package sample;

import javafx.fxml.Initializable;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.scene.layout.*;

import javafx.scene.image.Image;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.application.Application;
import javafx.geometry.Rectangle2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class ExpandedPhotoController implements Initializable {

    private Stage stage;
    private Scene scene;
    private Parent root;

    private UserList userList;
    private User user;
    private Album album;
    private Photo photo;

    public ExpandedPhotoController(UserList userList, User user, Album album, Photo photo) {
        this.userList = userList;
        this.user = user;
        this.album = album;
        this.photo = photo;
    }

    @FXML
    private ImageView image;

    @FXML
    private Label caption;

    @FXML
    private Label date;

    @FXML
    private VBox tagBox;

    @FXML
    private ChoiceBox<String> albumBox;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        try {
            image.setImage(new Image(new FileInputStream(photo.getImageFile())));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        caption.setText(photo.getCaption());
        date.setText(photo.getDate());
        addTags();

        if (!(user.getNumOfAlbums() == 1)) {
            for (Album a: user.getAlbumList()) {
                if (!a.getName().equals(album.getName()))
                    albumBox.getItems().add(a.getName());
            }
        }
    }

    public void addTags() {
        ArrayList<Tag> tags = photo.getTags();
        for (Tag t: tags) {
            Label tagName = new Label(t.getTagName());
            ArrayList<String> tagValues = t.getTagValues();
            VBox tagValueBox = new VBox();
            for (String value: tagValues) {
                Label valueLabel = new Label(value);
                tagValueBox.getChildren().add(valueLabel);
            }
            HBox tagEntry = new HBox(tagName, tagValueBox);
            tagEntry.setSpacing(10);
            tagEntry.setPadding(new Insets(5, 5, 5, 5));
            tagBox.getChildren().add(tagEntry);
        }
    }

    @FXML private void editPhoto(ActionEvent event) throws IOException {
        EditPhotoController editPhotoController = new EditPhotoController(userList, user, album, photo);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("editphoto.fxml"));
        loader.setController(editPhotoController);

        //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
        root = loader.load();
        //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML private void delete(ActionEvent event) throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you'd like to delete this photo?" , ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            album.removePhoto(photo);
            UserList.setUsers(userList);

            PhotoController photoController = new PhotoController(userList, user, album);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("photos.fxml"));
            loader.setController(photoController);

            //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
            root = loader.load();
            //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
        }
    }

    @FXML private void done(ActionEvent event) throws IOException {
        PhotoController photoController = new PhotoController(userList, user, album);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("photos.fxml"));
        loader.setController(photoController);

        //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
        root = loader.load();
        //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }

    @FXML
    public void copy() throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to copy this photo to: " + albumBox.getValue(), ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            if (albumBox.getValue().equals("")) {
                Alert wrong = new Alert(Alert.AlertType.ERROR, "Please select an album from dropdown before copying.", ButtonType.OK);
                wrong.showAndWait();
            } else {
                Album copyTo = user.getAlbum(albumBox.getValue());
                boolean worked = copyTo.addPhoto(photo);
                if (!worked) {
                    Alert wrong = new Alert(Alert.AlertType.ERROR, "Photo already exists in chosen album.", ButtonType.OK);
                    wrong.showAndWait();
                }
                UserList.setUsers(userList);
            }
        }

    }

    @FXML
    public void move() throws IOException {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to copy this photo to: " + albumBox.getValue(), ButtonType.YES, ButtonType.CANCEL);
        alert.showAndWait();

        if (alert.getResult() == ButtonType.YES) {
            if (albumBox.getValue().equals("")) {
                Alert wrong = new Alert(Alert.AlertType.ERROR, "Please select an album from dropdown before moving.", ButtonType.OK);
                wrong.showAndWait();
            } else {
                Album copyTo = user.getAlbum(albumBox.getValue());
                boolean worked = copyTo.addPhoto(photo);
                if (!worked) {
                    Alert wrong = new Alert(Alert.AlertType.ERROR, "Photo already exists in chosen album.", ButtonType.OK);
                    wrong.showAndWait();

                    Alert remove = new Alert(Alert.AlertType.CONFIRMATION, "Would you still like to remove the photo from the album", ButtonType.YES, ButtonType.NO);
                    wrong.showAndWait();

                    if (remove.getResult() == ButtonType.YES) {
                        album.removePhoto(photo);
                    }
                } else {
                    album.removePhoto(photo);
                }
                UserList.setUsers(userList);
            }
        }

    }

    @FXML private void next(ActionEvent event) throws IOException {
        Photo nextPhoto = album.getNextPhoto(photo);
        if (nextPhoto == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "No next photo", ButtonType.OK);
            alert.showAndWait();
        } else {
            ExpandedPhotoController expandedPhotoController = new ExpandedPhotoController(userList, user, album, nextPhoto);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("expandedphoto.fxml"));
            loader.setController(expandedPhotoController);

            //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
            root = loader.load();
            //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
        }
    }

    @FXML private void prev(ActionEvent event) throws IOException {
        Photo prevPhoto = album.getPrevPhoto(photo);
        if (prevPhoto == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR, "No previous photo", ButtonType.OK);
            alert.showAndWait();
        } else {
            ExpandedPhotoController expandedPhotoController = new ExpandedPhotoController(userList, user, album, prevPhoto);

            FXMLLoader loader = new FXMLLoader(getClass().getResource("expandedphoto.fxml"));
            loader.setController(expandedPhotoController);

            //root = FXMLLoader.load(getClass().getResource("albums.fxml"));
            root = loader.load();
            //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            //stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            scene = new Scene(root);
            stage.setScene(scene);
            stage.centerOnScreen();
            stage.show();
        }
    }
}
